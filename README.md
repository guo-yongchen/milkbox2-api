# 技术栈

## 主要技术

基础语言（Java 21）

整合框架（SpringBoot 3）

持久层框架（MybatisPlus）

缓存（Redis）

## 辅助技术

简化类（Lombok）

接口文档（SpringDoc）

糊涂工具包（Hutool）

权限认证（SaToken）

密码保护（BCrypt）

# 升级java21的问题

升级到java21以后的启动参数，不加会报错

启动举例

```shell
java21的bin目录/java -jar jar包目录/jar包名称.jar --add-opens java.base/sun.util.calendar=ALL-UNNAMED
```

必选参数

```shell
--add-opens java.base/sun.util.calendar=ALL-UNNAMED
```

这些以后可能会用到

```shell
--add-opens java.base/java.lang=ALL-UNNAMED 
--add-opens java.base/java.io=ALL-UNNAMED 
--add-opens java.base/java.math=ALL-UNNAMED 
--add-opens java.base/java.net=ALL-UNNAMED 
--add-opens java.base/java.nio=ALL-UNNAMED 
--add-opens java.base/java.security=ALL-UNNAMED 
--add-opens java.base/java.text=ALL-UNNAMED 
--add-opens java.base/java.time=ALL-UNNAMED 
--add-opens java.base/java.util=ALL-UNNAMED 
--add-opens java.base/jdk.internal.access=ALL-UNNAMED 
--add-opens java.base/jdk.internal.misc=ALL-UNNAMED
```

# 打包与启动

## 如何打jar包

在项目根路径下执行maven的package命令，之后会在milkbox-app模块下的target目录下生成一个jar包，可以使用指令启动这个jar包

## 在idea中启动

启动时需要添加环境变量，如下图所示

![image-20240112111124776](img/image-20240112111124776.png)

主要包含的环境变量

```bash
DB_HOST=localhost
DB_PASSWORD=xxxx
DB_PORT=3306
DB_USERNAME=root
```

## 在命令行中启动

启动命令参考

win，注意要使用cmd，不要用powershell

```bash
chcp
chcp 65001
C:\Users\Administrator\.jdks\openjdk-21.0.1\bin\java -Dfile.encoding=UTF-8 -Dsun.stdout.encoding=UTF-8 -Dsun.stderr.encoding=UTF-8 --add-opens java.base/sun.util.calendar=ALL-UNNAMED -jar .\milkbox-app-1.0-DEV.jar
```

# 新建模块

## 在milkbox-service模块下新建模块后需要配置maven的导包依赖

在项目根目录下的pom.xml的dependencyManagement中定义新模块的版本，然后在milkbox-app模块中引入新模块