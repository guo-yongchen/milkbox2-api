package top.milkbox.log.modular.main.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.milkbox.common.annotation.CommonLog;
import top.milkbox.common.enums.LogTypeEnum;
import top.milkbox.common.validation.annotation.CollectionElementNotNull;
import top.milkbox.log.core.config.LogConfiguration;
import top.milkbox.log.modular.main.param.LogMainAddParam;
import top.milkbox.log.modular.main.param.LogMainEditParam;
import top.milkbox.log.modular.main.param.LogMainIdParam;
import top.milkbox.log.modular.main.param.LogMainPageParam;
import top.milkbox.log.modular.main.service.LogMainService;
import top.milkbox.log.modular.main.vo.LogMainVo;
import top.milkbox.common.pojo.CommonResult;

import java.util.List;

/**
 * 日志主表（log_main）控制器
 *
 * @author milkbox
 * @date 2024-1-23
 */
@SaCheckLogin
@RestController
@AllArgsConstructor
@RequestMapping("/logMain")
@Tag(name = "日志主表控制器", description = "LogMainController")
public class LogMainController {

    private LogMainService logMainService;

//    @PostMapping("/add")
//    @Operation(summary = "添加", description = "添加一条数据")
//    @CommonLog(value = "添加", description = "添加一条数据",
//            module = LogConfiguration.MODULE_NAME, type = LogTypeEnum.INSERT)
//    public CommonResult<Object> add(@Validated @RequestBody LogMainAddParam addParam) {
//        logMainService.add(addParam);
//        return CommonResult.ok();
//    }

    @DeleteMapping("/delete")
    @Operation(summary = "批量删除")
    @CommonLog(value = "批量删除", module = LogConfiguration.MODULE_NAME, type = LogTypeEnum.DELETE)
    public CommonResult<Object> delete(
            @RequestBody
            @Size(min = 1, message = "请至少传递一个删除对象")
            @CollectionElementNotNull(message = "集合中的删除对象不能为空")
            List<@Valid LogMainIdParam> paramList
    ) {
        logMainService.delete(paramList);
        return CommonResult.ok();
    }

//    @PutMapping("/edit")
//    @Operation(summary = "修改", description = "修改一条数据")
//    @CommonLog(value = "修改", description = "修改一条数据",
//            module = LogConfiguration.MODULE_NAME, type = LogTypeEnum.UPDATE)
//    public CommonResult<Object> edit(@Validated @RequestBody LogMainEditParam editParam) {
//        logMainService.edit(editParam);
//        return CommonResult.ok();
//    }

    @GetMapping("/detail")
    @Operation(summary = "详情", description = "查询一条数据的详情")
    @CommonLog(value = "详情", description = "查询一条数据的详情", saveResult = false,
            module = LogConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT) // 不保存返回值到数据库，防止套娃
    public CommonResult<LogMainVo> detail(@Validated LogMainIdParam idParam) {
        return CommonResult.ok(logMainService.detail(idParam));
    }

    @GetMapping("/page")
    @Operation(summary = "分页查询", description = "查询条件分页查询")
    @CommonLog(value = "分页查询", description = "查询条件分页查询", saveResult = false,
            module = LogConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT) // 不保存返回值到数据库，防止套娃
    public CommonResult<Page<LogMainVo>> page(@Validated LogMainPageParam pageParam) {
        return CommonResult.ok(logMainService.page(pageParam));
    }

}