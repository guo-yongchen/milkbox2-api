package top.milkbox.log.modular.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.milkbox.log.modular.main.entity.LogMainEntity;

/**
 * 日志_日志主表(log_main)表数据库访问层
 *
 * @author milkbox
 * @date 2024-1-18
 */
@Mapper
public interface LogMainMapper extends BaseMapper<LogMainEntity> {

}