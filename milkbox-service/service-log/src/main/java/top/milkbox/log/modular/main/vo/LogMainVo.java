package top.milkbox.log.modular.main.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.enums.LogCategoryEnum;
import top.milkbox.common.enums.LogLevelEnum;
import top.milkbox.common.enums.LogTypeEnum;
import top.milkbox.common.pojo.CommonVo;

import java.io.Serializable;

/**
 * 日志_日志主表默认vo
 *
 * @author milkbox
 * @date 2024-1-18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Schema(description = "LogMainEntity 日志_日志主表。")
public class LogMainVo extends CommonVo implements Serializable {

    /**
     * 主键
     */
    @Schema(title = "主键",
            description = "主键")
    private Integer id;

    /**
     * 日志分类;暂定为枚举类型，api日志还是其他方法的日志，取值api和method，具体还要考虑一下
     */
    @Schema(title = "日志分类",
            description = "日志分类。暂定为枚举类型，api日志还是其他方法的日志，取值api和method")
    private LogCategoryEnum category;

    /**
     * 日志名称;对日志做一个简单的描述
     */
    @Schema(title = "日志名称",
            description = "日志名称。对日志做一个简单的描述")
    private String name;

    /**
     * 日志级别;枚举类型，INFO，DEBUG，ERROR等
     */
    @Schema(title = "日志级别",
            description = "日志级别。枚举类型，INFO，DEBUG，ERROR等。默认值“'TRACE'”",
            example = "'TRACE'", defaultValue = "'TRACE'")
    private LogLevelEnum level;

    /**
     * 执行状态;1表示成功，0表示失败
     */
    @Schema(title = "执行状态",
            description = "执行状态。1表示成功，0表示失败。默认值“1”",
            example = "1", defaultValue = "1")
    private Integer status;

    /**
     * 日志的详细描述;如果报错，则保存报错堆栈
     */
    @Schema(title = "日志的详细描述",
            description = "日志的详细描述。如果报错，则保存报错堆栈")
    private String description;

    /**
     * 操作主机地址
     */
    @Schema(title = "操作主机地址",
            description = "操作主机地址")
    private String operationIp;

    /**
     * 操作的浏览器
     */
    @Schema(title = "操作的浏览器",
            description = "操作的浏览器")
    private String operationBrowser;

    /**
     * 操作系统
     */
    @Schema(title = "操作系统",
            description = "操作系统")
    private String operationSystem;

    /**
     * 请求地址;如果是api日志
     */
    @Schema(title = "请求地址",
            description = "请求地址。如果是api日志")
    private String requestUrl;

    /**
     * 参数json类型
     */
    @Schema(title = "参数json类型",
            description = "请求参数json类型")
    private Object paramJson;

    /**
     * 结果json类型
     */
    @Schema(title = "结果json类型",
            description = "响应对象json类型")
    private Object resultJson;

    /**
     * 模块名称
     */
    @Schema(title = "模块名称",
            description = "模块名称")
    private String module;

    /**
     * 类型;枚举
     */
    @Schema(title = "类型",
            description = "类型。枚举")
    private LogTypeEnum type;

}