package top.milkbox.log.core.config;

import cn.hutool.core.thread.ExecutorBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 创建时间: 2024-01-22 下午 3:13
 *
 * @author milkbox
 */
@Slf4j
@Configuration
public class LogConfiguration {
    public static final String MODULE_NAME = "日志模块";

    /**
     * 初始线程5，最大线程10，线程超过10以后等待，等待队列最大100个
     * 主要用于异步保存日志信息
     *
     * @return 返回一个线程池
     */
    @Bean("logExecutorService")
    public ExecutorService createExecutorService() {
        log.info("创建日志线程池......");
        return ExecutorBuilder.create()
                .setCorePoolSize(5)
                .setMaxPoolSize(10)
                .setWorkQueue(new LinkedBlockingQueue<>(100))
                .build();
    }
}
