package top.milkbox.log.modular.main.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.aspectj.lang.ProceedingJoinPoint;
import top.milkbox.log.modular.main.entity.LogMainEntity;
import top.milkbox.log.modular.main.param.LogMainAddParam;
import top.milkbox.log.modular.main.param.LogMainEditParam;
import top.milkbox.log.modular.main.param.LogMainIdParam;
import top.milkbox.log.modular.main.param.LogMainPageParam;
import top.milkbox.log.modular.main.vo.LogMainVo;

import java.util.List;

/**
 * 日志_日志主表（log_main）服务层接口
 *
 * @author milkbox
 * @date 2024-1-18
 */
public interface LogMainService extends IService<LogMainEntity> {

    /**
     * 添加
     *
     * @param addParam 添加参数
     */
    void add(LogMainAddParam addParam);

    /**
     * 删除
     *
     * @param paramList 删除id对象集合
     */
    void delete(List<LogMainIdParam> paramList);

//    /**
//     * 通过id编辑
//     *
//     * @param editParam 编辑参数
//     */
//    void edit(LogMainEditParam editParam);

    /**
     * 通过id查询详情
     *
     * @param idParam id参数
     * @return 返回查询的详情，如果没有则返回空
     */
    LogMainVo detail(LogMainIdParam idParam);

    /**
     * 查询实体，即简单查询，包含存在性校验，不存在报业务异常
     *
     * @param entityId 实体id
     * @return 返回实体
     */
    LogMainEntity findEntity(Integer entityId);

    /**
     * 分页查询
     *
     * @param pageParam 分页查询参数
     * @return 返回苞米豆的分页对象，没有数据，则record长度为0
     */
    Page<LogMainVo> page(LogMainPageParam pageParam);

    /**
     * 异步保存日志，保存正常日志
     *
     * @param joinPoint 切点
     * @param result    方法的返回值
     */
    void saveNormalLogAsync(ProceedingJoinPoint joinPoint, Object result);

    /**
     * 异步保存报错日志，保存异常日志
     *
     * @param joinPoint 切点
     * @param throwable 报错信息
     */
    void saveThrowableLogAsync(ProceedingJoinPoint joinPoint, Throwable throwable);

}