package top.milkbox.sys.modular.relationship.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.milkbox.sys.modular.relationship.enums.SysRelationshipTypeEnum;

import java.util.Set;

/**
 * targetIdList集合授权给objectId<br />
 * 创建时间: 2024-04-08 下午 3:07
 *
 * @author milkbox
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRelationshipObjectAuthTargetBo {

    /**
     * targetIdList集合授权给objectId
     */
    private Integer objectId;

    /**
     * targetIdList集合授权给objectId
     */
    private Set<Integer> targetIdList;

    /**
     * 是否重新授权
     */
    private Boolean isReauthorization;

    /**
     * 关联的类型;枚举
     */
    private SysRelationshipTypeEnum category;
}
