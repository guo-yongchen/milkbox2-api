package top.milkbox.sys.modular.relationship.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.milkbox.common.annotation.CommonLog;
import top.milkbox.common.enums.LogTypeEnum;
import top.milkbox.common.validation.annotation.CollectionElementNotNull;
import top.milkbox.sys.core.config.SysConfiguration;
import top.milkbox.sys.modular.relationship.param.SysRelationshipAddParam;
import top.milkbox.sys.modular.relationship.param.SysRelationshipEditParam;
import top.milkbox.sys.modular.relationship.param.SysRelationshipIdParam;
import top.milkbox.sys.modular.relationship.param.SysRelationshipPageParam;
import top.milkbox.sys.modular.relationship.service.SysRelationshipService;
import top.milkbox.sys.modular.relationship.vo.SysRelationshipVo;
import top.milkbox.common.pojo.CommonResult;

import java.util.List;

/**
 * 关系表（sys_relationship）控制器
 *
 * @author milkbox
 * @date 2024-1-27
 */
@SaCheckLogin
@RestController
@AllArgsConstructor
@RequestMapping("/sysRelationship")
@Tag(name = "关系表控制器", description = "SysRelationshipController")
public class SysRelationshipController {

    private SysRelationshipService sysRelationshipService;

//    @PostMapping("/add")
//    @Operation(summary = "添加", description = "添加一条数据")
//    @CommonLog(value = "添加", description = "添加一条数据",
//            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.INSERT)
//    public CommonResult<Object> add(@Validated @RequestBody SysRelationshipAddParam addParam) {
//        sysRelationshipService.add(addParam);
//        return CommonResult.ok();
//    }
//
//    @DeleteMapping("/delete")
//    @Operation(summary = "批量删除")
//    @CommonLog(value = "批量删除", module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.DELETE)
//    public CommonResult<Object> delete(
//            @RequestBody
//            @Size(min = 1, message = "请至少传递一个删除对象")
//            @CollectionElementNotNull(message = "集合中的删除对象不能为空")
//            List<@Valid SysRelationshipIdParam> paramList
//    ) {
//        sysRelationshipService.delete(paramList);
//        return CommonResult.ok();
//    }
//
//    @PutMapping("/edit")
//    @Operation(summary = "修改", description = "修改一条数据")
//    @CommonLog(value = "修改", description = "修改一条数据",
//            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.UPDATE)
//    public CommonResult<Object> edit(@Validated @RequestBody SysRelationshipEditParam editParam) {
//        sysRelationshipService.edit(editParam);
//        return CommonResult.ok();
//    }
//
//    @GetMapping("/detail")
//    @Operation(summary = "详情", description = "查询一条数据的详情")
//    @CommonLog(value = "详情", description = "查询一条数据的详情",
//            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
//    public CommonResult<SysRelationshipVo> detail(@Validated SysRelationshipIdParam idParam) {
//        return CommonResult.ok(sysRelationshipService.detail(idParam));
//    }
//
//    @GetMapping("/page")
//    @Operation(summary = "分页查询", description = "查询条件分页查询")
//    @CommonLog(value = "分页查询", description = "查询条件分页查询",
//            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
//    public CommonResult<Page<SysRelationshipVo>> page(@Validated SysRelationshipPageParam pageParam) {
//        return CommonResult.ok(sysRelationshipService.page(pageParam));
//    }

}