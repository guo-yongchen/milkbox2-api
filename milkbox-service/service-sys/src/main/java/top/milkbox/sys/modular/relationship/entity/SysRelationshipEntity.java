package top.milkbox.sys.modular.relationship.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.pojo.CommonEntity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import top.milkbox.sys.modular.relationship.enums.SysRelationshipTypeEnum;

/**
 * 系统_关系表_用户角色组织菜单权限综合关系表
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_relationship", autoResultMap = true)
@Schema(description = "SysRelationshipEntity 系统_关系表_用户角色组织菜单权限综合关系表。")
public class SysRelationshipEntity extends CommonEntity implements Serializable {

    public SysRelationshipEntity(Integer objectId, Integer targetId, SysRelationshipTypeEnum category) {
        this.objectId = objectId;
        this.targetId = targetId;
        this.category = category;
    }

    public SysRelationshipEntity(Integer objectId, Integer targetId, SysRelationshipTypeEnum category, Object extend) {
        this.objectId = objectId;
        this.targetId = targetId;
        this.category = category;
        this.extend = extend;
    }

    /**
     * 主键
     */
    @TableId
    @Schema(title = "主键",
            description = "主键")
    private Integer id;

    /**
     * 对象id;被关联的id
     */
    @Schema(title = "对象id",
            description = "对象id。被关联的id")
    private Integer objectId;

    /**
     * 目标id;关联到的目标id
     */
    @Schema(title = "目标id",
            description = "目标id。关联到的目标id")
    private Integer targetId;

    /**
     * 关联的类型;枚举
     */
    @Schema(title = "关联的类型",
            description = "关联的类型。枚举")
    private SysRelationshipTypeEnum category;

    /**
     * 扩展信息;Json格式
     */
    @Schema(title = "扩展信息",
            description = "扩展信息。Json格式")
    @TableField(typeHandler = JacksonTypeHandler.class, updateStrategy = FieldStrategy.ALWAYS)
    private Object extend;

}