package top.milkbox.sys.modular.menu.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.milkbox.common.annotation.CommonLog;
import top.milkbox.common.enums.LogTypeEnum;
import top.milkbox.common.validation.annotation.CollectionElementNotNull;
import top.milkbox.sys.core.config.SysConfiguration;
import top.milkbox.sys.modular.menu.param.SysMenuAddParam;
import top.milkbox.sys.modular.menu.param.SysMenuEditParam;
import top.milkbox.sys.modular.menu.param.SysMenuIdParam;
import top.milkbox.sys.modular.menu.param.SysMenuPageParam;
import top.milkbox.sys.modular.menu.service.SysMenuService;
import top.milkbox.sys.modular.menu.vo.SysMenuVo;
import top.milkbox.common.pojo.CommonResult;

import java.util.List;

/**
 * 菜单表（sys_menu）控制器
 *
 * @author milkbox
 * @date 2024-1-29
 */
@SaCheckLogin
@RestController
@AllArgsConstructor
@RequestMapping("/sysMenu")
@Tag(name = "菜单表控制器", description = "SysMenuController")
public class SysMenuController {

    private SysMenuService sysMenuService;

    @PostMapping("/add")
    @Operation(summary = "添加", description = "添加一条数据")
    @CommonLog(value = "添加", description = "添加一条数据",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.INSERT)
    public CommonResult<Object> add(@Validated @RequestBody SysMenuAddParam addParam) {
        sysMenuService.add(addParam);
        return CommonResult.ok();
    }

    @DeleteMapping("/delete")
    @Operation(summary = "普通批量删除")
    @CommonLog(value = "普通批量删除", module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.DELETE)
    public CommonResult<Object> delete(
            @RequestBody
            @Size(min = 1, message = "请至少传递一个删除对象")
            @CollectionElementNotNull(message = "集合中的删除对象不能为空")
            List<@Valid SysMenuIdParam> paramList
    ) {
        sysMenuService.delete(paramList);
        return CommonResult.ok();
    }

    @DeleteMapping("/replaceDelete")
    @Operation(summary = "取代删除")
    @CommonLog(value = "取代删除", module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.DELETE)
    public CommonResult<Object> replaceDelete(@Validated @RequestBody SysMenuIdParam idParam) {
        sysMenuService.replaceDelete(idParam);
        return CommonResult.ok();
    }

    @DeleteMapping("/cascadingDelete")
    @Operation(summary = "级联删除")
    @CommonLog(value = "级联删除", module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.DELETE)
    public CommonResult<Object> cascadingDelete(@Validated @RequestBody SysMenuIdParam idParam) {
        sysMenuService.cascadingDelete(idParam);
        return CommonResult.ok();
    }

    @PutMapping("/edit")
    @Operation(summary = "修改", description = "修改一条数据")
    @CommonLog(value = "修改", description = "修改一条数据",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.UPDATE)
    public CommonResult<Object> edit(@Validated @RequestBody SysMenuEditParam editParam) {
        sysMenuService.edit(editParam);
        return CommonResult.ok();
    }

    @GetMapping("/detail")
    @Operation(summary = "详情", description = "查询一条数据的详情")
    @CommonLog(value = "详情", description = "查询一条数据的详情",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
    public CommonResult<SysMenuVo> detail(@Validated SysMenuIdParam idParam) {
        return CommonResult.ok(sysMenuService.detail(idParam));
    }

//    @GetMapping("/page")
//    @Operation(summary = "分页查询", description = "查询条件分页查询")
//    @CommonLog(value = "分页查询", description = "查询条件分页查询",
//            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
//    public CommonResult<Page<SysMenuVo>> page(@Validated SysMenuPageParam pageParam) {
//        return CommonResult.ok(sysMenuService.page(pageParam));
//    }

    @GetMapping("/forest")
    @Operation(summary = "获取菜单森林", description = "获取当前登录用户拥有权限的菜单森林")
    @CommonLog(value = "获取菜单森林", description = "获取当前登录用户拥有权限的菜单森林",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
    public CommonResult<List<Tree<Integer>>> forest() {
        return CommonResult.ok(sysMenuService.forest());
    }

    // TODO 应设较高权限
    // TODO 这里的菜单应该具有条件查询功能，在查询的时候需要注意通过结果查询其上级，最终构建一棵树
    @GetMapping("/forestAll")
    @Operation(summary = "获取所有菜单森林", description = "获取所有菜单森林")
    @CommonLog(value = "获取所有菜单森林", description = "获取所有菜单森林",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
    public CommonResult<List<Tree<Integer>>> forestAll() {
        return CommonResult.ok(sysMenuService.forestAll());
    }

}