package top.milkbox.sys.modular.role.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.pojo.CommonVo;

import java.io.Serializable;

/**
 * 系统_角色表默认vo
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Schema(description = "SysRoleEntity 系统_角色表。")
public class SysRoleVo extends CommonVo implements Serializable {

    /**
     * 主键
     */
    @Schema(title = "主键",
            description = "主键")
    private Integer id;

    /**
     * 角色名
     */
    @Schema(title = "角色名",
            description = "角色名")
    private String name;

    /**
     * 角色值
     */
    @Schema(title = "角色值",
            description = "角色值")
    private String value;

}