package top.milkbox.sys.modular.relationship.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.milkbox.common.bo.EntityColumnExistence;
import top.milkbox.sys.modular.relationship.bo.SysRelationshipObjectAuthTargetBo;
import top.milkbox.sys.modular.relationship.bo.SysRelationshipTargetAuthObjectBo;
import top.milkbox.sys.modular.relationship.entity.SysRelationshipEntity;
import top.milkbox.sys.modular.relationship.enums.SysRelationshipTypeEnum;
import top.milkbox.sys.modular.relationship.param.*;
import top.milkbox.sys.modular.relationship.vo.SysRelationshipVo;
import top.milkbox.sys.modular.role.param.SysRoleAuthorizationMenuParam;
import top.milkbox.sys.modular.role.param.SysRoleAuthorizationUserParam;
import top.milkbox.sys.modular.user.param.SysUserAuthorizationMenuParam;
import top.milkbox.sys.modular.user.param.SysUserAuthorizationRoleParam;

import java.util.List;

/**
 * 系统_关系表_用户角色组织菜单权限综合关系表（sys_relationship）服务层接口
 *
 * @author milkbox
 * @date 2024-1-23
 */
public interface SysRelationshipService extends IService<SysRelationshipEntity> {

    /**
     * 添加
     *
     * @param addParam 添加参数
     */
    void add(SysRelationshipAddParam addParam);

    /**
     * 删除
     *
     * @param paramList 删除id对象集合
     */
    void delete(List<SysRelationshipIdParam> paramList);

    /**
     * 通过id编辑
     *
     * @param editParam 编辑参数
     */
    void edit(SysRelationshipEditParam editParam);

    /**
     * 通过id查询详情
     *
     * @param idParam id参数
     * @return 返回查询的详情，如果没有则返回空
     */
    SysRelationshipVo detail(SysRelationshipIdParam idParam);

    /**
     * 查询实体，即简单查询，包含存在性校验，不存在报业务异常
     *
     * @param entityId 实体id
     * @return 返回实体
     */
    SysRelationshipEntity findEntity(Integer entityId);

    /**
     * 查询object关联的targetId集合<br />
     * object在前，target在后
     *
     * @param objectId objectId
     * @param type     关系类型，若类型为空，匹配所有类型
     */
    List<Integer> findTargetIdListByObjectId(Integer objectId, SysRelationshipTypeEnum type);

    /**
     * 查询target关联的objectId集合<br />
     * object在前，target在后
     *
     * @param targetId targetId
     * @param type     关系类型，若类型为空，匹配所有类型
     */
    List<Integer> findObjectIdListByTargetId(Integer targetId, SysRelationshipTypeEnum type);

    /**
     * 通过object字段和类型字段删除<br />
     * object在前，target在后
     *
     * @param deleteObjectList 要删除的记录的Object字段集合
     * @param type             关系类型，若类型为空，匹配所有类型
     */
    void deleteByObjectAndType(List<Integer> deleteObjectList, SysRelationshipTypeEnum type);

    /**
     * 通过target字段和类型字段删除<br />
     * object在前，target在后
     *
     * @param deleteTargetList 要删除的记录的target字段集合
     * @param type             关系类型，若类型为空，匹配所有类型
     */
    void deleteByTargetAndType(List<Integer> deleteTargetList, SysRelationshipTypeEnum type);

    /**
     * objectId授权targetIdList
     *
     * @param objectAuthTargetBo    objectId授权targetIdList参数，包含objectId，targetIdList，isReauthorization以及category
     * @param entityColumnExistence 存在性校验对象，包含存在的实体id集合与不存在的实体id集合
     * @return 返回的对象包含两个集合，一个为授权成功的id集合，另一个为不存在的id集合
     */
    EntityColumnExistence<Integer> objectAuthTarget(
            SysRelationshipObjectAuthTargetBo objectAuthTargetBo, EntityColumnExistence<Integer> entityColumnExistence);

    /**
     * targetId授权objectIdList
     *
     * @param targetAuthObjectBo    targetId授权objectIdList参数，包含targetId，objectIdList，isReauthorization以及category
     * @param entityColumnExistence 存在性校验对象，包含存在的实体id集合与不存在的实体id集合
     * @return 返回的对象包含两个集合，一个为授权成功的id集合，另一个为不存在的id集合
     */
    EntityColumnExistence<Integer> targetAuthObject(
            SysRelationshipTargetAuthObjectBo targetAuthObjectBo, EntityColumnExistence<Integer> entityColumnExistence);

    /**
     * 角色授权用户集合
     *
     * @param param 包含角色id，用户id集合，是否重新授权
     * @return 返回的对象包含两个集合，一个为授权成功的id集合，另一个为不存在的id集合
     */
    EntityColumnExistence<Integer> roleIdAuthUserIdList(SysRoleAuthorizationUserParam param);

    /**
     * 角色授权菜单集合
     *
     * @param param 包含角色id，菜单id集合，是否重新授权
     * @return 返回的对象包含两个集合，一个为授权成功的id集合，另一个为不存在的id集合
     */
    EntityColumnExistence<Integer> roleIdAuthMenuIdList(SysRoleAuthorizationMenuParam param);

    /**
     * 用户授权菜单集合
     *
     * @param param 包含用户id，菜单id集合，是否重新授权
     * @return 返回的对象包含两个集合，一个为授权成功的id集合，另一个为不存在的id集合
     */
    EntityColumnExistence<Integer> userIdAuthMenuIdList(SysUserAuthorizationMenuParam param);

    /**
     * 用户授权角色集合
     *
     * @param param 包含用户id，角色id集合，是否重新授权
     * @return 返回的对象包含两个集合，一个为授权成功的id集合，另一个为不存在的id集合
     */
    EntityColumnExistence<Integer> userIdAuthRoleIdList(SysUserAuthorizationRoleParam param);
}