package top.milkbox.sys.modular.menu.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.milkbox.common.service.CommonService;
import top.milkbox.sys.modular.menu.entity.SysMenuEntity;
import top.milkbox.sys.modular.menu.param.SysMenuAddParam;
import top.milkbox.sys.modular.menu.param.SysMenuEditParam;
import top.milkbox.sys.modular.menu.param.SysMenuIdParam;
import top.milkbox.sys.modular.menu.param.SysMenuPageParam;
import top.milkbox.sys.modular.menu.vo.SysMenuVo;

import java.util.List;

/**
 * 系统_菜单表（sys_menu）服务层接口
 *
 * @author milkbox
 * @date 2024-1-29
 */
public interface SysMenuService extends CommonService<SysMenuEntity> {

    /**
     * 添加
     *
     * @param addParam 添加参数
     */
    void add(SysMenuAddParam addParam);

    /**
     * 普通删除
     * 直接删除当前节点，不考虑后续子节点，在定时任务中自动清理不可达节点
     *
     * @param paramList 删除id对象集合
     */
    void delete(List<SysMenuIdParam> paramList);

    /**
     * 取代删除<br />
     * <b>出于性能考虑</b>，此操作仅支持单个删除
     *
     * @param idParam 删除这个id的菜单
     */
    void replaceDelete(SysMenuIdParam idParam);

    /**
     * 级联删除<br />
     * <b>出于性能考虑</b>，此操作仅支持单个删除<br />
     * <b>高耗时操作</b>。树越深，执行的查询越多，效率越低
     *
     * @param idParam 删除这个id的菜单以及其子节点
     */
    void cascadingDelete(SysMenuIdParam idParam);

    /**
     * 通过id编辑
     *
     * @param editParam 编辑参数
     */
    void edit(SysMenuEditParam editParam);

    /**
     * 通过id查询详情
     *
     * @param idParam id参数
     * @return 返回查询的详情，如果没有则返回空
     */
    SysMenuVo detail(SysMenuIdParam idParam);

    /**
     * 查询实体，即简单查询，包含存在性校验，不存在报业务异常
     *
     * @param entityId 实体id
     * @return 返回实体
     */
    SysMenuEntity findEntity(Integer entityId);


//    /**
//     * 分页查询
//     *
//     * @param pageParam 分页查询参数
//     * @return 返回苞米豆的分页对象，没有数据，则record长度为0
//     */
//    Page<SysMenuVo> page(SysMenuPageParam pageParam);

    /**
     * 获取当前登录用户拥有权限的菜单森林
     *
     * @return 菜单森林
     */
    List<Tree<Integer>> forest();

    /**
     * 获取所有菜单森林
     *
     * @return 菜单森林
     */
    List<Tree<Integer>> forestAll();
}