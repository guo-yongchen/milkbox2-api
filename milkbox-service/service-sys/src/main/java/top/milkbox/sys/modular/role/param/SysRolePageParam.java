package top.milkbox.sys.modular.role.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.pojo.CommonPageParam;

import java.io.Serializable;

/**
 * 分页参数对象
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysRolePageParam extends CommonPageParam implements Serializable {

    /**
     * 角色名
     */
    @Schema(title = "角色名",
            description = "角色名")
    private String name;

    /**
     * 角色值
     */
    @Schema(title = "角色值",
            description = "角色值")
    private String value;

}