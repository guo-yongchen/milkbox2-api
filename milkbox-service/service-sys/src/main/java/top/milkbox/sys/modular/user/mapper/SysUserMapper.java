package top.milkbox.sys.modular.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.milkbox.sys.modular.user.entity.SysUserEntity;

/**
 * 系统_用户表(sys_user)表数据库访问层
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

}