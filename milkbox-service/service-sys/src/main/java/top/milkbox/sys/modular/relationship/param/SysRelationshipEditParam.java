package top.milkbox.sys.modular.relationship.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import jakarta.validation.constraints.NotNull;
import top.milkbox.sys.modular.relationship.enums.SysRelationshipTypeEnum;

/**
 * 编辑参数对象
 *
 * @author milkbox
 * @date 2024-1-27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRelationshipEditParam implements Serializable {

    /**
     * 主键
     */
    @Schema(title = "主键",
            description = "主键")
    @NotNull(message = "主键不能为空")
    private Integer id;

    /**
     * 对象id;被关联的id
     */
    @Schema(title = "对象id",
            description = "对象id。被关联的id")
    @NotNull(message = "对象id不能为空")
    private Integer objectId;

    /**
     * 目标id;关联到的目标id
     */
    @Schema(title = "目标id",
            description = "目标id。关联到的目标id")
    @NotNull(message = "目标id不能为空")
    private Integer targetId;

    /**
     * 关联的类型;枚举
     */
    @Schema(title = "关联的类型",
            description = "关联的类型。枚举")
    @NotNull(message = "关联的类型不能为空")
    private SysRelationshipTypeEnum category;

    /**
     * 扩展信息;Json格式
     */
    @Schema(title = "扩展信息",
            description = "扩展信息。Json格式")
    private Object extend;

}