package top.milkbox.sys.modular.role.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.milkbox.common.validation.annotation.CollectionElementNotNull;

import java.util.Set;

/**
 * 角色授权用户<br />
 * 创建时间: 2024-01-29 下午 3:27
 *
 * @author milkbox
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRoleAuthorizationUserParam {

    @Schema(title = "角色id", description = "角色id")
    @NotNull(message = "角色id不能为空")
    private Integer roleId;

    @Schema(title = "用户id集合", description = """
            被授权的用户id集合。
            在重新授权模式下，如果此字段为空或集合个数为0，则表示删除原来的授权信息而不添加新的授权信息。""")
    @Size(max = 10000, message = "一次性授权的最大个数为10000")
    @CollectionElementNotNull(message = "集合中的用户id不能为空")
    private Set<Integer> userIdList;

    @Schema(title = "是否重新授权", description = """
            为true表示重新授权，false或空表示不重新授权。
            重新授权的意思是删除原来的授权信息重新授权新的集合。
            不重新授权就是增量授权，已经授权的不变，只添加新的授权。""")
    private Boolean isReauthorization;
}
