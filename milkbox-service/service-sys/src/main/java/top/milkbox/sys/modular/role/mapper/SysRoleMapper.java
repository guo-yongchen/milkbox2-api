package top.milkbox.sys.modular.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.milkbox.sys.modular.role.entity.SysRoleEntity;

/**
 * 系统_角色表(sys_role)表数据库访问层
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

}