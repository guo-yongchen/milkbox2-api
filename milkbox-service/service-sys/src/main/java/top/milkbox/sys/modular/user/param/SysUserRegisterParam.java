package top.milkbox.sys.modular.user.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.milkbox.sys.modular.user.enums.SysUserGenderEnum;
import top.milkbox.sys.modular.user.enums.SysUserStatusEnum;

import java.io.Serializable;
import java.util.Date;

/**
 * 注册参数对象
 *
 * @author milkbox
 * @date 2024-1-24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserRegisterParam implements Serializable {

    /**
     * 登录账号
     */
    @Schema(title = "登录账号",
            description = "登录账号")
    private String account;

    /**
     * 登录密码
     */
    @Schema(title = "登录密码",
            description = "登录密码")
    @NotBlank(message = "登录密码不能为空")
    private String password;

    /**
     * 昵称
     */
    @Schema(title = "昵称",
            description = "昵称")
    private String nickname;

    /**
     * 登录邮箱
     */
    @Schema(title = "登录邮箱",
            description = "登录邮箱")
    private String email;

    /**
     * 登录手机号
     */
    @Schema(title = "登录手机号",
            description = "登录手机号")
    private String phone;

    /**
     * 头像;base64格式
     */
    @Schema(title = "头像",
            description = "头像。base64格式")
    private String avatar;

    /**
     * 性别;枚举
     */
    @Schema(title = "性别",
            description = "性别。枚举")
    private SysUserGenderEnum gender;

}