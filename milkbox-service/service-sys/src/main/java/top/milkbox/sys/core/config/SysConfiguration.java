package top.milkbox.sys.core.config;

import org.springframework.context.annotation.Configuration;

/**
 * 创建时间: 2024-01-22 下午 3:43
 *
 * @author milkbox
 */
@Configuration
public class SysConfiguration {
    public static final String MODULE_NAME = "系统模块";
}
