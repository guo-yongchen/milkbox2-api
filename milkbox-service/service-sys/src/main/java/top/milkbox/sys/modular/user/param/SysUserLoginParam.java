package top.milkbox.sys.modular.user.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 登录参数对象
 *
 * @author milkbox
 * @date 2024-1-24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserLoginParam implements Serializable {

    /**
     * 登录账号
     */
    @Schema(title = "登录账号",
            description = "可以是账号、邮箱或手机号")
    @NotBlank(message = "账号不能为空")
    private String account;

    /**
     * 登录密码
     */
    @Schema(title = "登录密码",
            description = "登录密码")
    @NotBlank(message = "登录密码不能为空")
    private String password;

}