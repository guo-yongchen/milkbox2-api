package top.milkbox.sys.modular.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.milkbox.sys.modular.menu.entity.SysMenuEntity;

/**
 * 系统_菜单表(sys_menu)表数据库访问层
 *
 * @author milkbox
 * @date 2024-1-29
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {

}