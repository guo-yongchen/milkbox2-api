package top.milkbox.sys.modular.role.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.milkbox.common.annotation.CommonLog;
import top.milkbox.common.bo.EntityColumnExistence;
import top.milkbox.common.enums.LogTypeEnum;
import top.milkbox.common.validation.annotation.CollectionElementNotNull;
import top.milkbox.sys.core.config.SysConfiguration;
import top.milkbox.sys.modular.relationship.service.SysRelationshipService;
import top.milkbox.sys.modular.role.param.*;
import top.milkbox.sys.modular.role.service.SysRoleService;
import top.milkbox.sys.modular.role.vo.SysRoleVo;
import top.milkbox.common.pojo.CommonResult;

import java.util.List;

/**
 * 角色表（sys_role）控制器
 *
 * @author milkbox
 * @date 2024-1-27
 */
@SaCheckLogin
@RestController
@AllArgsConstructor
@RequestMapping("/sysRole")
@Tag(name = "角色表控制器", description = "SysRoleController")
public class SysRoleController {

    private SysRelationshipService sysRelationshipService;

    private SysRoleService sysRoleService;

    @PostMapping("/add")
    @Operation(summary = "添加", description = "添加一条数据")
    @CommonLog(value = "添加", description = "添加一条数据",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.INSERT)
    public CommonResult<Object> add(@Validated @RequestBody SysRoleAddParam addParam) {
        sysRoleService.add(addParam);
        return CommonResult.ok();
    }

    @DeleteMapping("/delete")
    @Operation(summary = "批量删除")
    @CommonLog(value = "批量删除", module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.DELETE)
    public CommonResult<Object> delete(
            @RequestBody
            @Size(min = 1, message = "请至少传递一个删除对象")
            @CollectionElementNotNull(message = "集合中的删除对象不能为空")
            List<@Valid SysRoleIdParam> paramList
    ) {
        sysRoleService.delete(paramList);
        return CommonResult.ok();
    }

    @PutMapping("/edit")
    @Operation(summary = "修改", description = "修改一条数据")
    @CommonLog(value = "修改", description = "修改一条数据",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.UPDATE)
    public CommonResult<Object> edit(@Validated @RequestBody SysRoleEditParam editParam) {
        sysRoleService.edit(editParam);
        return CommonResult.ok();
    }

    @GetMapping("/detail")
    @Operation(summary = "详情", description = "查询一条数据的详情")
    @CommonLog(value = "详情", description = "查询一条数据的详情",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
    public CommonResult<SysRoleVo> detail(@Validated SysRoleIdParam idParam) {
        return CommonResult.ok(sysRoleService.detail(idParam));
    }

    @GetMapping("/page")
    @Operation(summary = "分页查询", description = "查询条件分页查询")
    @CommonLog(value = "分页查询", description = "查询条件分页查询",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.SELECT)
    public CommonResult<Page<SysRoleVo>> page(@Validated SysRolePageParam pageParam) {
        return CommonResult.ok(sysRoleService.page(pageParam));
    }

    @PostMapping("/roleIdAuthUserIdList")
    @Operation(summary = "授权用户", description = "给角色授权授权用户")
    @CommonLog(value = "授权用户", description = "给角色授权授权用户",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.UPDATE)
    public CommonResult<String> roleIdAuthUserIdList(
            @Validated @RequestBody SysRoleAuthorizationUserParam roleAuthorizationMenuParam) {
        EntityColumnExistence<Integer> columnExistence =
                sysRelationshipService.roleIdAuthUserIdList(roleAuthorizationMenuParam);
        if (ObjectUtil.isEmpty(columnExistence.getDoesNotExistColumnList())) {
            return CommonResult.ok();
        }

        return CommonResult.ok("用户" + JSONUtil.toJsonStr(columnExistence.getExistColumnList()) + "已授权成功。"
                + "用户" + JSONUtil.toJsonStr(columnExistence.getDoesNotExistColumnList())
                + "由于不存在所以未授权。", null);
    }

    @PostMapping("/roleIdAuthMenuIdList")
    @Operation(summary = "授权菜单", description = "给角色授权授权菜单")
    @CommonLog(value = "授权菜单", description = "给角色授权授权菜单",
            module = SysConfiguration.MODULE_NAME, type = LogTypeEnum.UPDATE)
    public CommonResult<String> roleIdAuthMenuIdList(
            @Validated @RequestBody SysRoleAuthorizationMenuParam roleAuthorizationMenuParam) {
        EntityColumnExistence<Integer> columnExistence =
                sysRelationshipService.roleIdAuthMenuIdList(roleAuthorizationMenuParam);
        if (ObjectUtil.isEmpty(columnExistence.getDoesNotExistColumnList())) {
            return CommonResult.ok();
        }

        return CommonResult.ok("菜单" + JSONUtil.toJsonStr(columnExistence.getExistColumnList()) + "已授权成功。"
                + "菜单" + JSONUtil.toJsonStr(columnExistence.getDoesNotExistColumnList())
                + "由于不存在所以未授权。", null);
    }
}