package top.milkbox.sys.modular.relationship.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

/**
 * 关系表类型枚举<br />
 * 此类型用来标识关系表中object_id与target_id的关系<br />
 * 可以通过命名格式来确认谁是object_id，谁是target_id，一般在RELATE前面的是object_id，后面的是target_id<br />
 * 在数据库中object_id字段在前面，target_id在后面<br />
 * 创建时间: 2024-01-23 下午 4:28
 *
 * @author milkbox
 */
@AllArgsConstructor // 如果使用此注解，请勿随意修改成员变量的定义顺序
public enum SysRelationshipTypeEnum {

    /**
     * 用户与角色关系（用户属于哪些角色，用户为object_id，角色为target_id）
     */
    SYS_USER_RELATE_SYS_ROLE("用户与角色关系", "SYS_USER_RELATE_SYS_ROLE"),

    /**
     * 用户与菜单关系（用户单独拥有的菜单，用户为object_id，菜单为target_id）
     */
    SYS_USER_RELATE_SYS_MENU("用户与菜单关系", "SYS_USER_RELATE_SYS_MENU"),

    /**
     * 用户与组织关系（用户所属与哪些组织，用户为object_id，组织为target_id）
     */
    SYS_USER_RELATE_SYS_ORG("用户与组织关系", "SYS_USER_RELATE_SYS_ORG"),

    /**
     * 角色与菜单关系（角色拥有那些菜单，角色为object_id，菜单为target_id）
     */
    SYS_ROLE_RELATE_SYS_MENU("角色与菜单关系", "SYS_ROLE_RELATE_SYS_MENU");

    private final String label;

    @EnumValue // mybatis-plus控制数据库字段，数据库中保存的字段。如果数据库中的字段与不在枚举中则返回空
    private final String value;

    public String getLabel() {
        return label;
    }

    @JsonValue // jackson的标识，用于json转枚举的时候使用的字段
    public String getValue() {
        return value;
    }

    /**
     * 枚举对象转为json的时候，字段的值
     */
    @Override
    public String toString() {
        return value;
    }
}
