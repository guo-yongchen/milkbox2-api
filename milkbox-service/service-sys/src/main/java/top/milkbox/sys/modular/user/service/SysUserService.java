package top.milkbox.sys.modular.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.milkbox.common.service.CommonService;
import top.milkbox.sys.modular.user.entity.SysUserEntity;
import top.milkbox.sys.modular.user.param.*;
import top.milkbox.sys.modular.user.vo.SysUserVo;

import java.util.List;

/**
 * 系统_用户表（sys_user）服务层接口
 *
 * @author milkbox
 * @date 2024-1-23
 */
public interface SysUserService extends CommonService<SysUserEntity> {

    /**
     * 添加
     *
     * @param addParam 添加参数
     */
    void add(SysUserAddParam addParam);

    /**
     * 删除
     *
     * @param paramList 删除id对象集合
     */
    void delete(List<SysUserIdParam> paramList);

    /**
     * 通过id编辑
     *
     * @param editParam 编辑参数
     */
    void edit(SysUserEditParam editParam);

    /**
     * 通过id查询详情
     *
     * @param idParam id参数
     * @return 返回查询的详情，如果没有则返回空
     */
    SysUserVo detail(SysUserIdParam idParam);

    /**
     * 查询实体，即简单查询，包含存在性校验，不存在报业务异常
     *
     * @param entityId 实体id
     * @return 返回实体
     */
    SysUserEntity findEntity(Integer entityId);

    /**
     * 分页查询
     *
     * @param pageParam 分页查询参数
     * @return 返回苞米豆的分页对象，没有数据，则record长度为0
     */
    Page<SysUserVo> page(SysUserPageParam pageParam);

    /**
     * 通过账号、邮箱或手机号查询用户
     *
     * @param keyword 可以是账号、邮箱或手机号
     * @return 返回查询到的用户信息，如果没有查询到则返回空
     */
    SysUserEntity findByAccountKeyword(String keyword);

    /**
     * 注册
     *
     * @param registerParam 注册参数
     * @return 返回注册用户的部分信息
     */
    SysUserVo register(SysUserRegisterParam registerParam);

    /**
     * 登录
     *
     * @param loginParam 登录参数
     * @return 返回登录用户的token以及部分用户信息
     */
    SysUserVo login(SysUserLoginParam loginParam);

    /**
     * 获取当前登录的用户信息
     *
     * @return 用户信息，如果用户未登录则返回空
     */
    SysUserVo getLoginUser();

    /**
     * 登出当前用户
     */
    void logout();
}