package top.milkbox.sys.modular.role.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.NotBlank;

/**
 * 编辑参数对象
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRoleEditParam implements Serializable {

    /**
     * 主键
     */
    @Schema(title = "主键",
            description = "主键")
    @NotNull(message = "主键不能为空")
    private Integer id;

    /**
     * 角色名
     */
    @Schema(title = "角色名",
            description = "角色名")
    @NotBlank(message = "角色名不能为空")
    private String name;

    /**
     * 角色值
     */
    @Schema(title = "角色值",
            description = "角色值")
    private String value;

}