package top.milkbox.sys.modular.user.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.milkbox.common.validation.annotation.CollectionElementNotNull;

import java.util.Set;

/**
 * 用户授权菜单<br />
 * 创建时间: 2024-04-08
 *
 * @author milkbox
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserAuthorizationMenuParam {

    @Schema(title = "用户id", description = "用户id")
    @NotNull(message = "用户id不能为空")
    private Integer userId;

    @Schema(title = "菜单id集合", description = """
            被授权的菜单id集合。
            在重新授权模式下，如果此字段为空或集合个数为0，则表示删除原来的授权信息而不添加新的授权信息。""")
    @Size(max = 10000, message = "一次性授权的最大个数为10000")
    @CollectionElementNotNull(message = "集合中的菜单id不能为空")
    private Set<Integer> menuIdList;

    @Schema(title = "是否重新授权", description = """
            为true表示重新授权，false或空表示不重新授权。
            重新授权的意思是删除原来的授权信息重新授权新的集合。
            不重新授权就是增量授权，已经授权的不变，只添加新的授权。""")
    private Boolean isReauthorization;
}
