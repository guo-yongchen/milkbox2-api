package top.milkbox.sys.modular.menu.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.pojo.CommonVo;
import top.milkbox.sys.modular.menu.entity.SysMenuEntity;
import top.milkbox.sys.modular.menu.enums.SysMenuTypeEnum;

import java.io.Serializable;

/**
 * 系统_菜单表默认vo
 *
 * @author milkbox
 * @date 2024-1-29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Schema(description = "SysMenuEntity 系统_菜单表。")
public class SysMenuVo extends CommonVo implements Serializable {

    /**
     * 主键
     */
    @Schema(title = "主键",
            description = "主键")
    private Integer id;

    /**
     * 上级id
     */
    @Schema(title = "上级id",
            description = "上级id")
    private Integer parentId;

    /**
     * 中文名
     */
    @Schema(title = "中文名",
            description = "中文名")
    private String title;

    /**
     * 组件唯一别名
     */
    @Schema(title = "组件唯一别名",
            description = "组件唯一别名")
    private String name;

    /**
     * 组件的导包路径;相对于组件的基准目录，基准目录单独配置，开头不加反斜杠
     */
    @Schema(title = "组件的导包路径",
            description = "组件的导包路径。相对于组件的基准目录，基准目录单独配置，开头不加反斜杠")
    private String componentPath;

    /**
     * 相对于父级的路由地址;开头要加反斜杠，可以多级
     */
    @Schema(title = "相对于父级的路由地址",
            description = "相对于父级的路由地址。开头要加反斜杠，可以多级")
    private String path;

    /**
     * 重定向地址;当这个字段不为空表示有重定向，开启重定向后component字段失效
     */
    @Schema(title = "重定向地址",
            description = "重定向地址。当这个字段不为空表示有重定向，开启重定向后component字段失效")
    private String redirect;

    /**
     * 扩展信息;Json格式
     */
    @Schema(title = "扩展信息",
            description = "扩展信息。Json格式")
    private Object extend;

    /**
     * 布局页面的组件名称。布局页面的组件名称，表示当前记录在哪个布局页面内。仅顶级有效
     */
    @Schema(title = "布局页面的组件名称",
            description = "布局页面的组件名称。布局页面的组件名称，表示当前记录在哪个布局页面内。仅顶级有效")
    private String layoutName;

    /**
     * 是否可见;隐藏后可以访问页面，但不在菜单列表显示。1可见，0不可见
     */
    @Schema(title = "是否可见",
            description = "是否可见。隐藏后可以访问页面，但不在菜单列表显示。true可见，false不可见，null不可见")
    private Boolean isShow;

    /**
     * 类型;页面PAGE或目录CATALOG
     */
    @Schema(title = "类型",
            description = "类型;页面PAGE或目录CATALOG")
    private SysMenuTypeEnum type;

    /**
     * 图标
     */
    @Schema(title = "图标",
            description = "图标")
    private String icon;

    /**
     * 主题颜色
     */
    @Schema(title = "主题颜色",
            description = "主题颜色")
    private String color;

}