package top.milkbox.sys.modular.relationship.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.milkbox.sys.modular.relationship.entity.SysRelationshipEntity;

/**
 * 系统_关系表_用户角色组织菜单权限综合关系表(sys_relationship)表数据库访问层
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Mapper
public interface SysRelationshipMapper extends BaseMapper<SysRelationshipEntity> {

}