package top.milkbox.sys.modular.user.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

/**
 * 用户状态枚举
 * 创建时间: 2024-01-23 下午 4:28
 *
 * @author milkbox
 */
@AllArgsConstructor // 如果使用此注解，请勿随意修改成员变量的定义顺序
public enum SysUserStatusEnum {
    NORMAL("正常", "NORMAL"),

    /**
     * 管理员手动禁用账户
     */
    DISABLED("禁用", "DISABLED"),

    /**
     * 账户本人手动删除账户，并非登出账号
     */
    CANCEL("注销", "CANCEL"),

    /**
     * 管理员手动删除账户，区分逻辑删除，此处为防止关联查询异常的删除
     */
    DELETED("删除", "DELETED");

    private final String label;

    @EnumValue // mybatis-plus控制数据库字段，数据库中保存的字段。如果数据库中的字段与不在枚举中则返回空
    private final String value;

    public String getLabel() {
        return label;
    }

    @JsonValue // jackson的标识，用于json转枚举的时候使用的字段
    public String getValue() {
        return value;
    }

    /**
     * 枚举对象转为json的时候，字段的值
     */
    @Override
    public String toString() {
        return value;
    }
}
