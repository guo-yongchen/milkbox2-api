package top.milkbox.sys.modular.user.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import jakarta.validation.constraints.NotNull;
import top.milkbox.sys.modular.user.enums.SysUserGenderEnum;
import top.milkbox.sys.modular.user.enums.SysUserStatusEnum;

import java.util.Date;

/**
 * 编辑参数对象
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserEditParam implements Serializable {

    /**
     * 主键
     */
    @Schema(title = "主键",
            description = "主键")
    @NotNull(message = "主键不能为空")
    private Integer id;

    /**
     * 登录账号
     */
    @Schema(title = "登录账号",
            description = "登录账号")
    private String account;

    /**
     * 登录密码
     */
    @Schema(title = "登录密码",
            description = "登录密码")
    @NotBlank(message = "登录密码不能为空")
    private String password;

    /**
     * 昵称
     */
    @Schema(title = "昵称",
            description = "昵称")
    private String nickname;

    /**
     * 登录邮箱
     */
    @Schema(title = "登录邮箱",
            description = "登录邮箱")
    private String email;

    /**
     * 登录手机号
     */
    @Schema(title = "登录手机号",
            description = "登录手机号")
    private String phone;

    /**
     * 头像;base64格式
     */
    @Schema(title = "头像",
            description = "头像。base64格式")
    private String avatar;

    /**
     * 性别;枚举
     */
    @Schema(title = "性别",
            description = "性别。枚举")
    private SysUserGenderEnum gender;

    /**
     * 用户密级;字典表value
     */
    @Schema(title = "用户密级",
            description = "用户密级。字典表value")
    private String secretLevel;

    /**
     * 用户状态;枚举
     */
    @Schema(title = "用户状态",
            description = "用户状态。枚举")
    private SysUserStatusEnum status;

    /**
     * 上次登录ip
     */
    @Schema(title = "上次登录ip",
            description = "上次登录ip")
    private String beforeLoginIp;

    /**
     * 上次登录地址
     */
    @Schema(title = "上次登录地址",
            description = "上次登录地址")
    private String beforeLoginAddress;

    /**
     * 上次登录时间
     */
    @Schema(title = "上次登录时间",
            description = "上次登录时间")
    private Date beforeLoginTime;

    /**
     * 上次登录设备
     */
    @Schema(title = "上次登录设备",
            description = "上次登录设备")
    private String beforeLoginDevice;

    /**
     * 最新登录ip
     */
    @Schema(title = "最新登录ip",
            description = "最新登录ip")
    private String newLoginIp;

    /**
     * 最新登录地址
     */
    @Schema(title = "最新登录地址",
            description = "最新登录地址")
    private String newLoginAddress;

    /**
     * 最新登录时间
     */
    @Schema(title = "最新登录时间",
            description = "最新登录时间")
    private Date newLoginTime;

    /**
     * 最新登录设备
     */
    @Schema(title = "最新登录设备",
            description = "最新登录设备")
    private String newLoginDevice;

}