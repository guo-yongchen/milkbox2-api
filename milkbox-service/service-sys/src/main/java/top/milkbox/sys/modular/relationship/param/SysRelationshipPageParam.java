package top.milkbox.sys.modular.relationship.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.pojo.CommonPageParam;
import top.milkbox.sys.modular.relationship.enums.SysRelationshipTypeEnum;

import java.io.Serializable;

/**
 * 分页参数对象
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysRelationshipPageParam extends CommonPageParam implements Serializable {

    /**
     * 对象id;被关联的id
     */
    @Schema(title = "对象id",
            description = "对象id。被关联的id")
    private Integer objectId;

    /**
     * 目标id;关联到的目标id
     */
    @Schema(title = "目标id",
            description = "目标id。关联到的目标id")
    private Integer targetId;

    /**
     * 关联的类型;枚举
     */
    @Schema(title = "关联的类型",
            description = "关联的类型。枚举")
    private SysRelationshipTypeEnum category;

    /**
     * 扩展信息;Json格式
     */
    @Schema(title = "扩展信息",
            description = "扩展信息。Json格式")
    private Object extend;

}