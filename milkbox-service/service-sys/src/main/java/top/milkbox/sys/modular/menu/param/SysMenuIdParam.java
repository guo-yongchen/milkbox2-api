package top.milkbox.sys.modular.menu.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 主键参数对象
 *
 * @author milkbox
 * @date 2024-1-29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysMenuIdParam implements Serializable {

    /**
     * 主键
     */
    @Schema(title = "主键",
            description = "主键")
    @NotNull(message = "主键不能为空")
    private Integer id;

}