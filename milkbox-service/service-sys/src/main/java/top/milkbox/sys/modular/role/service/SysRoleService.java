package top.milkbox.sys.modular.role.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import top.milkbox.common.bo.EntityColumnExistence;
import top.milkbox.common.service.CommonService;
import top.milkbox.sys.modular.role.entity.SysRoleEntity;
import top.milkbox.sys.modular.role.param.*;
import top.milkbox.sys.modular.role.vo.SysRoleVo;

import java.util.List;

/**
 * 系统_角色表（sys_role）服务层接口
 *
 * @author milkbox
 * @date 2024-1-23
 */
public interface SysRoleService extends CommonService<SysRoleEntity> {

    /**
     * 添加
     *
     * @param addParam 添加参数
     */
    void add(SysRoleAddParam addParam);

    /**
     * 删除
     *
     * @param paramList 删除id对象集合
     */
    void delete(List<SysRoleIdParam> paramList);

    /**
     * 通过id编辑
     *
     * @param editParam 编辑参数
     */
    void edit(SysRoleEditParam editParam);

    /**
     * 通过id查询详情
     *
     * @param idParam id参数
     * @return 返回查询的详情，如果没有则返回空
     */
    SysRoleVo detail(SysRoleIdParam idParam);

    /**
     * 查询实体，即简单查询，包含存在性校验，不存在报业务异常
     *
     * @param entityId 实体id
     * @return 返回实体
     */
    SysRoleEntity findEntity(Integer entityId);

    /**
     * 分页查询
     *
     * @param pageParam 分页查询参数
     * @return 返回苞米豆的分页对象，没有数据，则record长度为0
     */
    Page<SysRoleVo> page(SysRolePageParam pageParam);

//    /**
//     * 重新给角色授权用户
//     *
//     * @param assignUserParam 角色id与用户id集合
//     */
//    EntityColumnExistence<Integer> reauthorizationUser(SysRoleAuthorizationUserParam assignUserParam);
}