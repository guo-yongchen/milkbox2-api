package top.milkbox.sys.modular.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.pojo.CommonEntity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import top.milkbox.sys.modular.user.enums.SysUserGenderEnum;
import top.milkbox.sys.modular.user.enums.SysUserStatusEnum;

import java.util.Date;

/**
 * 系统_用户表
 *
 * @author milkbox
 * @date 2024-1-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_user")
@Schema(description = "SysUserEntity 系统_用户表。")
public class SysUserEntity extends CommonEntity implements Serializable {

    /**
     * 主键
     */
    @TableId
    @Schema(title = "主键",
            description = "主键")
    private Integer id;

    /**
     * 登录账号
     */
    @Schema(title = "登录账号",
            description = "登录账号")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String account;

    /**
     * 登录密码
     */
    @Schema(title = "登录密码",
            description = "登录密码")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String password;

    /**
     * 昵称
     */
    @Schema(title = "昵称",
            description = "昵称")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String nickname;

    /**
     * 登录邮箱
     */
    @Schema(title = "登录邮箱",
            description = "登录邮箱")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String email;

    /**
     * 登录手机号
     */
    @Schema(title = "登录手机号",
            description = "登录手机号")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String phone;

    /**
     * 头像;base64格式
     */
    @Schema(title = "头像",
            description = "头像。base64格式")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String avatar;

    /**
     * 性别;枚举
     */
    @Schema(title = "性别",
            description = "性别。枚举")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private SysUserGenderEnum gender;

    /**
     * 用户密级;字典表value
     */
    @Schema(title = "用户密级",
            description = "用户密级。字典表value")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String secretLevel;

    /**
     * 用户状态;枚举
     */
    @Schema(title = "用户状态",
            description = "用户状态。枚举")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private SysUserStatusEnum status;

    /**
     * 上次登录ip
     */
    @Schema(title = "上次登录ip",
            description = "上次登录ip")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String beforeLoginIp;

    /**
     * 上次登录地址
     */
    @Schema(title = "上次登录地址",
            description = "上次登录地址")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String beforeLoginAddress;

    /**
     * 上次登录时间
     */
    @Schema(title = "上次登录时间",
            description = "上次登录时间")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private Date beforeLoginTime;

    /**
     * 上次登录设备
     */
    @Schema(title = "上次登录设备",
            description = "上次登录设备")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String beforeLoginDevice;

    /**
     * 最新登录ip
     */
    @Schema(title = "最新登录ip",
            description = "最新登录ip")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String newLoginIp;

    /**
     * 最新登录地址
     */
    @Schema(title = "最新登录地址",
            description = "最新登录地址")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String newLoginAddress;

    /**
     * 最新登录时间
     */
    @Schema(title = "最新登录时间",
            description = "最新登录时间")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private Date newLoginTime;

    /**
     * 最新登录设备
     */
    @Schema(title = "最新登录设备",
            description = "最新登录设备")
    @TableField(updateStrategy = FieldStrategy.ALWAYS)
    private String newLoginDevice;

}