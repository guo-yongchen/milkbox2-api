package top.milkbox.modular.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import top.milkbox.modular.entity.SysRole;
import top.milkbox.common.pojo.CommonPageParam;
import top.milkbox.common.pojo.CommonResult;

/**
 * 创建时间: 2023-12-27 下午 2:48
 *
 * @author milkbox
 */
@RestController
@RequestMapping("/test")
@Tag(name = "测试控制器", description = "TestController")
public class TestController {

    @GetMapping("/test")
    @Operation(summary = "测试接口", description = "测试接口详细描述")
    public CommonResult<SysRole> test(
            @RequestParam(required = false) @Parameter(description = "参数1") String param) {
        SysRole sysRole = new SysRole();
        sysRole.setName(param);
        return CommonResult.ok(sysRole);
    }

    @GetMapping("/page")
    @Operation(summary = "分页测试", description = "分页测试详细描述")
    public CommonResult<CommonPageParam> page(@Parameter(description = "参数1") CommonPageParam pageParam) {
        return CommonResult.ok(pageParam);
    }

}