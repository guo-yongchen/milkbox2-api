package top.milkbox.modular.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import top.milkbox.common.pojo.CommonEntity;

import java.io.Serializable;

/**
 * 系统_角色表
 *
 * @author milkbox
 * @date 2023-12-31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role")
@Schema(title = "SysRole", description = "系统_角色表。")
public class SysRole extends CommonEntity implements Serializable {

    /**
     * 主键
     */
    @TableId
    @Schema(title = "主键")
    private String id;

    /**
     * 角色名
     */
    @Schema(title = "角色名")
    private String name;

    /**
     * 角色值
     */
    @Schema(title = "角色值")
    private String value;

}