package top.milkbox;

import cn.hutool.core.date.DateTime;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import top.milkbox.common.pojo.CommonResult;

import java.util.Date;
import java.util.TimeZone;

/**
 * springboot启动类，注意这个类必须在top.milkbox包下面，不能再往里了
 * 创建时间: 2023-12-26 下午 5:54
 *
 * @author milkbox
 */
@Slf4j
// 注意：使用satoken不能加这个注解，否则在SaServletFilter中调用SpringMVCUtil.getRequest()“报错非Web上下文无法获取Request”
//@EnableWebMvc
@RestController
@SpringBootApplication
public class Application {

    /**
     * 主启动函数
     */
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(Application.class);
//        springApplication.setBannerMode(Banner.Mode.OFF); // 禁用boot横幅
        ConfigurableApplicationContext app = springApplication.run(args);
        log.info("当前服务器时间（new Date()）与时区（TimeZone.getDefault()）：" +
                new Date() + "，" + JSONUtil.toJsonStr(TimeZone.getDefault()));
        log.info("当前服务器公共字段默认注入时间（DateTime.now()）：" + DateTime.now());
//        log.info("通用配置：\n" + JSONUtil.toJsonPrettyStr(app.getBean("commonProperties")));
        log.info("文档地址：swagger-ui/index.html");
        log.info(">>> {}", Application.class.getSimpleName().toUpperCase() + " STARTING SUCCESS 启动成功");
    }

    @GetMapping("/")
    public CommonResult<String> welcome(@RequestParam(required = false) String message) {
        return CommonResult.ok("系统运行中......", message);
    }
}