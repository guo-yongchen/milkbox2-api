package top.milkbox.common.exceprion;

import cn.hutool.core.util.StrUtil;

/**
 * 自定义服务异常
 * 一般用于业务逻辑中出现的业务错误，返回给前端提示
 * 创建时间: 2024-01-08 下午 4:39
 *
 * @author milkbox
 */
public class CommonServiceException extends RuntimeException {


    public CommonServiceException() {
        super("未知业务逻辑异常");
    }

    public CommonServiceException(String message) {
        super(message);
    }

    public CommonServiceException(String msg, Object... arguments) {
        super(StrUtil.format(msg, arguments));
    }
}
