package top.milkbox.common.utils;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 创建时间: 2024-01-23 上午 10:40
 *
 * @author milkbox
 */
public class NetUtil {


    /**
     * 获取用户的真实ip地址
     *
     * @param request request对象
     * @return 返回StringIp
     */
    public static String getRealIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
    }
}
