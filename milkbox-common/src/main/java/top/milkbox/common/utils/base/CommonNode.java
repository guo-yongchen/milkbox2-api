package top.milkbox.common.utils.base;

import java.util.List;

public interface CommonNode<T> {

    /**
     * 获取节点的唯一标识
     */
    T getId();

    /**
     * 获取父节点的唯一标识
     */
    T getParentId();

    /**
     * 获取子节点列表
     */
    <S extends CommonNode<T>> List<S> getChildrenList();

    /**
     * 初始化子节点列表
     * <pre>
     *     // 一般只需要这么写
     *     public void initChildrenList() {
     *         this.childrenList = new ArrayList<>();
     *     }
     * </pre>
     */
    void initChildrenList();
}