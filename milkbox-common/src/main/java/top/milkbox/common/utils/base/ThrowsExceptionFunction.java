package top.milkbox.common.utils.base;

import java.util.Objects;

/**
 * 可以抛出异常的Function，由{@link java.util.function.Function}改造而来
 *
 * @param <T> 参数类型
 */
@FunctionalInterface
public interface ThrowsExceptionFunction<T, R> {

    R apply(T t) throws Exception;

    default <V> ThrowsExceptionFunction<V, R> compose(ThrowsExceptionFunction<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }

    default <V> ThrowsExceptionFunction<T, V> andThen(ThrowsExceptionFunction<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }

    static <T> ThrowsExceptionFunction<T, T> identity() {
        return t -> t;
    }
}
