package top.milkbox.common.utils.base;

import cn.hutool.core.util.ObjectUtil;

public class CycleRefException extends Exception {
    public CycleRefException(String message) {
        super(ObjectUtil.isEmpty(message) ? "循环引用异常" : message);
    }
}
