package top.milkbox.common.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;

/**
 * 删除标志枚举，1表示删除，0表示未删除
 *
 * @author milkbox
 */
//@Getter
@AllArgsConstructor
public enum CommonDeleteFlagEnum implements IEnum<Integer> {
    DELETED(1),
    NOT_DELETE(0);

    private final Integer value;

    @Override
    public Integer getValue() {
        return value;
    }
}
