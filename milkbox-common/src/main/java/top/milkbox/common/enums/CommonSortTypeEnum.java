package top.milkbox.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import top.milkbox.common.exceprion.CommonServiceException;

/**
 * 排序方式枚举
 *
 * @author milkbox
 */
@Getter
@AllArgsConstructor
public enum CommonSortTypeEnum {
    /**
     * 升序
     */
    ASC("ASC"),
    /**
     * 降序
     */
    DESC("DESC");

    private final String value;

    /**
     * 校验是否符合枚举
     *
     * @param value 被校验的值
     */
    public static void validate(String value) {
        value = value.toUpperCase();
        if (!ASC.getValue().equals(value) && !DESC.getValue().equals(value)) {
            throw new CommonServiceException("不支持该排序方式：{}", value);
        }
    }
}
