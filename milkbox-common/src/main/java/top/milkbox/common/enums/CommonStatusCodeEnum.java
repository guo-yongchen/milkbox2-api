package top.milkbox.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 状态码枚举
 * 可参考类：cn.hutool.http.HttpStatus
 *
 * @author milkbox
 */
@Getter
@AllArgsConstructor
public enum CommonStatusCodeEnum {
    OK200("200", "请求成功"),
    ERROR401("401", "未登录"),
    ERROR403("403", "无权限"),
    ERROR404("404", "路径不存在"),
    ERROR405("405", "请求方法不正确"),
    ERROR415("415", "参数类型错误"),
    ERROR500("500", "未知异常"),
    ERROR506("506", "业务逻辑异常");

    private final String code;
    private final String message;
}
