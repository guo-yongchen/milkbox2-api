package top.milkbox.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

/**
 * 日志分类枚举
 * 创建时间: 2024-01-19 下午 2:35
 *
 * @author milkbox
 */
@AllArgsConstructor // 如果使用此注解，请勿随意修改成员变量的定义顺序
public enum LogCategoryEnum {

    /**
     * 表示日志是接口类型的
     */
    API("接口", "API"),

    /**
     * 表示日志是一个普通方法的
     */
    METHOD("方法", "METHOD");

    private final String label;

    @EnumValue // mybatis-plus控制数据库字段，数据库中保存的字段。如果数据库中的字段与不在枚举中则返回空
    private final String value;

    public String getLabel() {
        return label;
    }

    @JsonValue // jackson的标识，用于json转枚举的时候使用的字段
    public String getValue() {
        return value;
    }

    /**
     * 枚举对象转为json的时候，字段的值
     */
    @Override
    public String toString() {
        return value;
    }
}
