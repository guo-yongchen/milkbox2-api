package top.milkbox.common.pojo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import top.milkbox.common.enums.CommonSortTypeEnum;

import java.io.Serializable;

/**
 * 公共分页对象
 *
 * @author milkbox
 */
@Data
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class CommonPageParam implements Serializable {

    public static final Integer DEFAULT_CURRENT = 1;
    public static final Integer DEFAULT_SIZE = 20;
    public static final Integer MAX_SIZE = 100; // 最大查询条数

    /**
     * 当前页
     */
    @Schema(title = "当前页码", description = "当前页码", example = "1")
    @DecimalMin(value = "1", message = "最小页码不能小于1")
    private Integer current = DEFAULT_CURRENT;

    /**
     * 每页条数
     */
    @Schema(title = "每页条数", description = "每页条数", example = "20")
    @DecimalMax(value = "100", message = "最大条数不能超过100")
    @DecimalMin(value = "1", message = "最小条数不能小于1")
    private Integer size = DEFAULT_SIZE;

    /**
     * 排序字段
     */
    @Schema(title = "排序字段", description = "排序字段，字段驼峰名称")
    private String sortField;

    /**
     * 排序方式
     */
    @Schema(title = "排序方式", description = "排序方式，升序：ASC；降序：DESC",
            example = "ASC", allowableValues = {"ASC", "DESC"})
    private CommonSortTypeEnum sortType;

    /**
     * 关键词
     */
    @Schema(title = "查询关键词", description = "查询关键词")
    private String searchKey;

    /**
     * 将当前对象转换为苞米豆的分页对象
     *
     * @param <T> entity类型
     * @return 返回苞米豆的分页对象
     */
    public <T> Page<T> toBaomidouPage() {
        return new Page<>(getCurrent(), getSize());
    }

}
