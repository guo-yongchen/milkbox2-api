package top.milkbox.common.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import top.milkbox.common.enums.CommonStatusCodeEnum;

import java.io.Serializable;

/**
 * 通用响应对象
 *
 * @param <ResultType> 响应数据的类型
 * @author milkbox
 */
@Data
@With
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<ResultType> implements Serializable {

    public static final String DEFAULT_MESSAGE = "操作成功";

    @Schema(title = "操作代码", example = "200")
    private String code;

    @Schema(title = "操作消息", example = DEFAULT_MESSAGE, defaultValue = DEFAULT_MESSAGE)
    private String message;

    @Schema(description = "返回的数据")
    private ResultType data;

    public static <ResultType> CommonResult<ResultType> create() {
        return new CommonResult<>();
    }

    public static <ResultType> CommonResult<ResultType> ok() {
        return CommonResult.ok(null);
    }

    public static <ResultType> CommonResult<ResultType> ok(ResultType data) {
        return CommonResult.ok(DEFAULT_MESSAGE, data);
    }

    public static <ResultType> CommonResult<ResultType> ok(String message, ResultType data) {
        return new CommonResult<>(CommonStatusCodeEnum.OK200.getCode(), message, data);
    }

    public static <ResultType> CommonResult<ResultType> error() {
        return CommonResult.error(CommonStatusCodeEnum.ERROR500.getMessage());
    }

    public static <ResultType> CommonResult<ResultType> error(String message) {
        return new CommonResult<>(CommonStatusCodeEnum.ERROR500.getCode(), message, null);
    }
}
