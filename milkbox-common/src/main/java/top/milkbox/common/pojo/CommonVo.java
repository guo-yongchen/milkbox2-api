package top.milkbox.common.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 公共vo字段
 *
 * @author milkbox
 */
@Data
public class CommonVo implements Serializable {

    @Schema(title = "排序字段")
    private Integer sortCode;

    /**
     * 创建人
     */
    @Schema(title = "创建人")
    private Integer createUser;

    /**
     * 创建时间
     */
    @Schema(title = "创建时间")
    private Date createTime;

    /**
     * 更新人
     */
    @Schema(title = "更新人")
    private Integer updateUser;

    /**
     * 更新时间
     */
    @Schema(title = "更新时间")
    private Date updateTime;
}
