package top.milkbox.common.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import top.milkbox.common.enums.CommonDeleteFlagEnum;

import java.io.Serializable;
import java.util.Date;

/**
 * 公共entity字段
 *
 * @author milkbox
 */
@Data
public class CommonEntity implements Serializable {

    /**
     * 排序字段
     */
    @Schema(title = "排序字段")
    private Integer sortCode;

    /**
     * 删除标志（NOT_DELETE；DELETED）
     */
    @TableLogic
    @Schema(title = "删除标志（1表示删除，0表示未删除）")
    @TableField(fill = FieldFill.INSERT)
    private CommonDeleteFlagEnum deleteFlag;
//    private Integer deleteFlag;

    /**
     * 创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @Schema(title = "创建人")
    private Integer createUser;

    /**
     * 创建时间
     */
    @Schema(title = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新人
     */
    @Schema(title = "更新人")
    @TableField(fill = FieldFill.UPDATE)
    private Integer updateUser;

    /**
     * 更新时间
     */
    @Schema(title = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;
}
