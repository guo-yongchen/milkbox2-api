package top.milkbox.common.annotation;

import top.milkbox.common.enums.LogLevelEnum;
import top.milkbox.common.enums.LogTypeEnum;

import java.lang.annotation.*;

/**
 * 日志注解
 * 创建时间: 2024-01-19 下午 2:06
 *
 * @author milkbox
 */
@Documented
@Target(ElementType.METHOD) // 仅作用于方法上
@Retention(RetentionPolicy.RUNTIME)
public @interface CommonLog {

    /**
     * 日志名称
     */
    String value() default "";

    /**
     * 日志级别
     *
     * @return 默认返回INFO级别
     */
    LogLevelEnum level() default LogLevelEnum.INFO;

    /**
     * 日志的详细描述
     */
    String description() default "";

    /**
     * 模块名称
     */
    String module() default "";

    /**
     * 日志的类型
     *
     * @return 默认返回空
     */
    LogTypeEnum type() default LogTypeEnum.NOT;

    /**
     * 是否保存函数的参数<br />
     * 在某些涉密操作中，可将此值改为false来阻止秘密信息被写入日志表，以保护隐私
     *
     * @return 默认情况下保存
     */
    boolean saveParam() default true;

    /**
     * 是否保存函数的返回值<br />
     * 在某些涉密操作中，可将此值改为false来阻止秘密信息被写入日志表，以保护隐私<br />
     * 在对日志表本身进行操作的时候，应当设置此字段为false，防止result字段出现套娃情况
     *
     * @return 默认情况下保存
     */
    boolean saveResult() default true;
}
