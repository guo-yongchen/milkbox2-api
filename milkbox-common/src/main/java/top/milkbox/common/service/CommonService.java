package top.milkbox.common.service;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.service.IService;
import top.milkbox.common.bo.EntityColumnExistence;

import java.io.Serializable;
import java.util.Collection;

/**
 * 抽取通用服务层接口<br />
 * 创建时间: 2024-02-04 下午 3:09
 *
 * @param <E> 实体类类型
 * @author milkbox
 */
public interface CommonService<E> extends IService<E> {

    /**
     * 过滤出在数据库中不存在的实体列<br />
     * 注意：此方法中包含一次数据库查询
     *
     * @param column                 被操作的字段的get方法的引用
     * @param entityColumnCollection 原始集合，如果其为空则返回的EntityColumnExistence中是两个长度为0的集合
     * @param <T>                    列的类型
     * @return 并返回一个包含存在和不存在实体列信息的对象
     */
    <T extends Serializable> EntityColumnExistence<T> filterDoesNotExistColumn(
            SFunction<E, T> column, Collection<T> entityColumnCollection);
}
