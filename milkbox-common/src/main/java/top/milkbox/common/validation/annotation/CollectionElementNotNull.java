package top.milkbox.common.validation.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import top.milkbox.common.validation.validator.CollectionElementNotNullValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

// TODO 请参照此注解编写更多的自定义校验，例如手机号、邮箱等

/**
 * 自定义校验注解<br />
 * 校验集合中的每一个元素都不为空<br />
 * 如果校验对象为空、集合元素为0或集合中的所有元素都不为空则校验通过<br />
 * 创建时间: 2024-01-29 下午 2:22
 *
 * @author milkbox
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CollectionElementNotNullValidator.class) // 校验的逻辑处理类
public @interface CollectionElementNotNull {

    /**
     * 提示的信息
     *
     * @return 默认值：集合中不能出现空元素
     */
    String message() default "集合中不能出现空元素";

    /**
     * 分组验证
     *
     * @return 默认值：{}
     */
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
