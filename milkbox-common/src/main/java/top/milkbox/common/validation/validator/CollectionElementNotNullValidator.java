package top.milkbox.common.validation.validator;

import cn.hutool.core.util.ObjectUtil;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import top.milkbox.common.validation.annotation.CollectionElementNotNull;

import java.util.Collection;

/**
 * 创建时间: 2024-01-29 下午 2:27
 *
 * @author milkbox
 */
public class CollectionElementNotNullValidator implements ConstraintValidator<CollectionElementNotNull, Collection<?>> {

    @Override
    public boolean isValid(Collection<?> value, ConstraintValidatorContext context) {
        if (ObjectUtil.isNotEmpty(value)) {
            return value.stream().noneMatch(ObjectUtil::isNull);
        }
        return true;
    }
}
