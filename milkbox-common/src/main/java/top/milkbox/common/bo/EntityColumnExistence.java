package top.milkbox.common.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 存在性类，对应数据库的同一列中哪些是存在的字段，哪些是不存在的字段<br />
 * 创建时间: 2024-02-04 下午 1:48
 *
 * @param <T> 列的类型
 * @author milkbox
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EntityColumnExistence<T> {

    /**
     * 存在的列
     */
    private List<T> existColumnList;

    /**
     * 不存在的列
     */
    private List<T> doesNotExistColumnList;
}
